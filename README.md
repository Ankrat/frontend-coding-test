## Overview:

This small coding test is an opportunity for you to show us your frontend knowledge. 
What we will be looking at is how your code is stuctured and the approch your take. 
We except the final solution to be fully responsive, you can use Firefox or Chrome for testing. 
If you have any questions regarding the excercice, please contact us.


## Exercise:

Fork the following repository:
[blackwelllearning/frontend-coding-test](https://bitbucket.org/blackwelllearning/frontend-coding-test)

With these tools:

- sass preprocessor
- bootstrap3 or foundation

You will create a single HTLM5 page that contains a few elements from the current Blackwell homepage ([http://bookshop.blackwell.co.uk](http://bookshop.blackwell.co.uk))

- a header
- a navbar
- a content section
- a footer


The header will contain the Blackwell logo [http://bookshop.blackwell.co.uk/assets/images2013/blackwell_logo.png](http://bookshop.blackwell.co.uk/assets/images2013/blackwell_logo.png) and a search box on the right.


The navbar links are:

- Home
- Business 
- Students
- Our Shops


Using javascript, display the last 5 articles from Hacker News in the content block.

Endpoint to get the last 100 items:
```bash
curl -i -H 'Accept: application/json' -X GET https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty
```

Endpoint to get the details of an item:
```bash
curl -i -H 'Accept: application/json' -X GET https://hacker-news.firebaseio.com/v0/item/9018839.json?print=pretty
```

Each article (item) will contain:

- 'title'
- 'text'
- 'time' (display nicely: i.e: mm/dd/yyyy hh:mm)
- 'type'

There should be a button above the list to sort the articles by title, or by date.


The footer is just a simple footer with a few links:

- Sitemap
- About Us
- Shops


There is no specific deadline for this although it should not take more than 1.5 hours to complete.


## Submission:

Commit your changes and push them on the repository.
Send us a link to your fork and you're all done!

