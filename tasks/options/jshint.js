module.exports = {
  beforeconcat: {
    src: ['js/*.js', '!js/*.min.js'],
    options: {
      jshintrc: '.jshintrc'
    }
  }
};