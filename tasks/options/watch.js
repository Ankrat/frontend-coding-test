module.exports = {
  options: {
    livereload: true,
  },
  html:{
    files: ['./*.html']
  },
  css:{
    files: ['./sass/**/*.scss'],
    tasks: ['sass:dev'],
    options: {
      spawn: false,
    }
  },
  scripts: {
    files: ['./js/**/*.js'],
    tasks: ['jshint'],
    options: {
      spawn: false,
    }
  }
}