module.exports = {
  connect: {
    options: {
      hostname: 'localhost',
      port: 9000,
      base: './',
      livereload: true,
      open: true
    }
  }
}