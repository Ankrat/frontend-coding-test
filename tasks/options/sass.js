module.exports = {
  options: {
    // enable the combass lib
    compass: true
  },
	dev: {
		options: {
			style: 'expanded',
      lineNumbers: true
		},
		files: {
      'css/base.css': './sass/style.scss',
			'css/bootstrap-theme.css': './sass/bootstrap/bootstrap-theme.scss'
		}
	},
  build: {
    options: {
      style: 'compact'
    },
    files: {
      'css/base.compact.css': './sass/style.scss',
      'css/bootstrap-theme.compact.css': './sass/bootstrap/bootstrap-theme.scss'
    }
  }
}