var app = {

  newsItem : [],
  // Hacker News get 100 Latest items
  getNews : function () {
    var urlNews = "https://hacker-news.firebaseio.com/v0/topstories.json",
        ajaxOptions = {
          url: urlNews,
          dataType: 'json',
          type: 'get'
        };

    $.ajax( ajaxOptions )
      .done( function( data ) {
        app.storeData( "newsList", data);
      });

    return;
  },

  // Provide a news id to get the detail
  getItem : function ( item ){
    var urlItem = "https://hacker-news.firebaseio.com/v0/item/" + item + ".json",
        ajaxOptions = {
          url: urlItem,
          dataType: 'json',
          type: 'get'
        };

    $.ajax( ajaxOptions )
      .done( function( data ) {
        app.storeItem( data );
        });


    return;
  },

  // Helper to store response from request in localStorage
  storeData : function ( key, val){
    localStorage.setItem( key, JSON.stringify( val ));
  },

  // Helper to access localStorage
  getData : function ( key ){
    return JSON.parse( localStorage.getItem( key ) );
  },

  storeItem: function( item ){
    app.newsItem.push( item );
    app.storeData( "newsItem", app.newsItem);
    // 5 News Items have been loaded
    // Then I render
    if( app.newsItem.length > 4 ){
      app.renderItems();
    }

  },

  renderItems: function(){
    var container = document.querySelector("#item-placeholder")

    _.each( app.newsItem, function( item ){
      var itemObj = item,
          el =  "",
          itemEl = document.createElement('section');

      itemEl.className = 'news-item';
      el = "<header>" +
             "<h4>" + itemObj.title + "</h4>" +
           "</header>" +
           "<section>" +
             "<p>" + itemObj.text + "</p>" +
           "</section>" +
           "<footer>" +
             "<p>" + app.unixToDate(itemObj.time) + "</p>" +
           "</footer>";

      itemEl.innerHTML = el;
      container.appendChild(itemEl);
    });

    return;
  },

  unixToDate: function( date ){
    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(date);

    // will display time in 10:30:23 format
    return( d );
  },

  sortTitle: function(e){
    var tmp = app.newsItem.sort( function (val1, val2) {
              // Convert value in Integer base 10
              var a = val1.title
                  b = val2.title;

              if (a < b) {
                return -1;
              }
              if (a  > b ) {
                return 1;
              }
              // a must be equal to b
              return 0;
            });

    app.newsItem = tmp;

    // Removing all children from an element
    var element = document.getElementById("item-placeholder");
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }

    app.renderItems();
    return;
  },

  sortDate: function(e){
    var tmp = app.newsItem.sort( function (val1, val2) {
              var a = parseInt(val1.time, 10),
                  b = parseInt(val2.time, 10);


              if (a < b) {
                return -1;
              }
              if (a  > b ) {
                return 1;
              }
              // a must be equal to b
              return 0;
            });
    app.newsItem = tmp;

    var element = document.getElementById("item-placeholder");
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }

    app.renderItems();
    return;
  }


};

$( document ).ready(function() {

  app.getNews();
  app.getData("newsItem");

  // Get the 100 latest ID
  var newsList = app.getData("newsList");

  // Array sorting IDs assuming that :
  // bigger the ID number is,
  // the more recent is the item
  var tmp = newsList.sort( function (val1, val2) {
              // Convert value in Integer base 10
              var a = parseInt(val1, 10),
                  b = parseInt(val2, 10);

              if (a < b) {
                return -1;
              }
              if (a  > b ) {
                return 1;
              }
              // a must be equal to b
              return 0;
            }),
      sortedNewsList = tmp.reverse().slice(0,5); // [1]

  // Fetching the 5 most recent news
  // Store them in localStorage
  _.each( sortedNewsList, function( item ){
    app.getItem( item );
  });

  // Bind filter button
  var sortTitle = document.querySelector('.js-sort-title');
  var sortDate = document.querySelector('.js-sort-date');
  sortTitle.addEventListener("click", app.sortTitle, false);
  sortDate.addEventListener("click", app.sortDate, false);
});


// 1:
// If 100 request is not a concern,
// removing the .slice(x,y) will load all 100 items